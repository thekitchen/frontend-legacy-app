// @ts-ignore
import template from "./honk-layout.template.html?raw";

/**
 *
 * @param $scope
 * @param tweetService
 */
function controller() {
  this.$onInit = () => {};
}

const component = {
  template,
  controller,
  bindings: {},
  transclude: {
    header: "?slotHeader",
    main: "slotMain",
  },
};

export default ["honkLayout", component];
