// @ts-ignore
import template from "./create-tweet.template.html?raw";

/**
 *
 * @param $scope
 */
function controller($scope) {
  this.$onInit = () => {
    this.message = "";
  };

  this.clearForm = () => {
    this.message = "";
  };

  this.submitTweet = () => {
    $scope.$emit("tweetCreated", { message: this.message });
    $scope.$apply(this.clearForm());
  };
}

controller.$inject = ["$scope"];

const component = {
  template,
  controller,
  bindings: {},
};

export default ["createTweet", component];
