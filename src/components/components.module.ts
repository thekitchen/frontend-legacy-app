import angular from "angular";
import createTweet from "./create-tweet/create-tweet.component";
import tweetCard from "./tweet-card/tweet-card.component";
import honkLayout from "./honk-layout/honk-layout.component";
import signForm from "./sign-form/sign-form.component";

angular
  .module("components", [])
  .component(...createTweet)
  .component(...tweetCard)
  .component(...honkLayout)
  .component(...signForm);
