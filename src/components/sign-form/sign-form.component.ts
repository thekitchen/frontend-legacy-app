// @ts-ignore
import template from "./sign-form.template.html?raw";

/**
 *
 * @param $scope
 * @param tweetService
 */
function controller($scope) {
  this.$onInit = () => {
    this.bgImg = {
      "background-image": `url(${this.img})`,
    };
    this.username = "";
    this.password = "";
  };

  this.clearForm = () => {
    this.username = "";
    this.password = "";
  };

  this.handleSubmit = () => {
    $scope.$emit("signSubmited", {
      username: this.username,
      password: this.password,
    });
    this.clearForm();
  };
}

const component = {
  template,
  controller,
  bindings: {
    formTitle: "@",
    btnLabel: "@",
    img: "@",
  },
  transclude: {
    slot: "?slot",
  },
};

component.$inject = ["$scope"];

export default ["signForm", component];
