/**
 * Sign up controller
 *
 * @param $scope
 * @param $window
 * @param accountService
 */
function signUpController($scope, $window, accountService) {
  /**
   * Register method
   *
   * @param event
   * @param data
   */
  function register(event, data) {
    accountService.register(data.username, data.password).then(() => {
      // eslint-disable-next-line no-param-reassign
      $window.location.href = "#/";
    });
  }

  $scope.$on("signSubmited", register);
}

signUpController.$inject = ["$scope", "$window", "accountService"];

export default ["signUpController", signUpController];
