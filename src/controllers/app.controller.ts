/**
 * @param $window
 */
function appController($window) {
  // eslint-disable-next-line no-param-reassign
  $window.location.href = "#/signin";
}

appController.$inject = ["$window"];

export default ["appController", appController];
