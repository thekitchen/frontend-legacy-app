import "angular";
import "angular-route";
import "angular-cookies";
import "@picocss/pico/css/pico.css";
import "./app.scss";

import "./app.module";
