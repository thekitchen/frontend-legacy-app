/**
 *
 */
function tweetService() {
  /**
   * Generate output date
   *
   * @param {Date} date input date
   * @returns {string} output
   */
  function formatDate(date: Date): string {
    return new Intl.DateTimeFormat("fr-FR", {
      weekday: "short",
      year: "numeric",
      month: "short",
      day: "numeric",
      hour: "2-digit",
      minute: "2-digit",
      second: "2-digit",
    }).format(date);
  }

  this.createTweet = (message: string, author: string) => {
    return fetch("http://localhost:8080/tweets", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        message,
        author,
      }),
    }).then((response) => {
      return response.json();
    });
  };

  this.likeTweet = (tweetId: string) => {
    return fetch(`http://localhost:8080/tweets/${tweetId}/like-tweet`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
    }).then((response) => {
      return response.json();
    });
  };

  this.listTweets = () => {
    return fetch("http://localhost:8080/tweets")
      .then((response) => {
        return response.json();
      })
      .then((jsonResp) => {
        const tweets = [];
        for (const tweet of jsonResp) {
          tweets.push({
            id: tweet.id,
            message: tweet.message,
            author: tweet.author,
            createdAt: formatDate(new Date(tweet.created_at)),
            likes: tweet.likes,
          });
        }
        return tweets.reverse();
      });
  };
}

export default ["tweetService", tweetService];
