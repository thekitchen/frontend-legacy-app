import angular from "angular";
import accountService from "./account.service";
import tweetService from "./tweet.service";

angular
  .module("services", [])
  .service(...accountService)
  .service(...tweetService);
