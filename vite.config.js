import { defineConfig } from "vite";

export default defineConfig({
  // config options
  root: "./src",
  base: "./",
  build: {
    outDir: process.cwd() + "/dist",
    emptyOutDir: true,
    sourcemap: true,
  },
  server: {
    open: true,
    proxy: {
      // string shorthand
      "/api": "http://localhost:8080",
    },
  },
});
